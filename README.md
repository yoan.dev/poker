# Poker
Pour lancer le programme, j'utilise codeBlock et le compilateur minGW.

Dans les différents choix: 
* Vilain a 2bet: 1 ==> Vous pouvez entrer un éventail entre 10 et 50 en nombre entier.
* Vilain a Call un 2Bet: 2 ==> Vous devez choisir un éventail par tranche de 5. Ex: 15, 25, 30...
* Vilain a 3Bet: 3 ==> Choisir parmis les valeurs indiquées.
* Vilain a Call un 3Bet: 4 ==> Choisir un éventail par tranche de 5 dans l'intervalle puis une des valeurs indiquées.

Les cartes du flop doivent être entré dans l'ordre décroissant.
Pour représenter A(As), K(roi), Q(dame), J(valet), T(Ten), on netera: 
* 14 = A
* 13 = K
* 12 = Q
* 11 = J
* 10 = T

Ainsi si je veux entrer le flop 3TQ je tape: 12 ensuite 10 et 3.

